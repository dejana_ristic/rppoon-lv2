﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class FileLogger:ILogger
    {
        private string filePath;
        public void fileLogger(string filepath)
        {
            this.filePath = filepath;
        }
        public void Log(ILoggable data) 
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath)) 
            {
                writer.WriteLine(data.GetStringRepresentation());
            }
        }
    }
}
