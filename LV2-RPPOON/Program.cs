﻿using System;

namespace LV2_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            //Random randomGenerator = new Random();
            for (int i = 0; i < 20; i++)
            {
                // diceRoller.InsertDie(new Die(6, randomGenerator));
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();
            diceRoller.GetRollingResults();
            /*foreach (int result in diceRoller.ResultForEachRoll)
            {
                Console.WriteLine(result);
            }
            */
            ConsoleLogger consoleLogger = new ConsoleLogger();
            consoleLogger.Log(diceRoller);
        }
    }
       
}
