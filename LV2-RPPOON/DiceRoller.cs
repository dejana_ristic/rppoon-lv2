﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class DiceRoller : ILoggable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger logger;
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }
        /*
        public void LogRollingResults()
        {
            foreach (int result in this.resultForEachRoll)
            {
                logger.Log(GetStringRepresentation());
            }
        }
        */
        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (int rollResult in this.resultForEachRoll)
            {
                stringBuilder.Append(results.ToString());
                stringBuilder.Append("\n");
            }
            return stringBuilder.ToString();
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
        public List<int> ResultForEachRoll
        {
            get { return resultForEachRoll; }
        }
        public void SetLogger(ILogger logger)
        {
            this.logger = logger;
        }
    }
}
