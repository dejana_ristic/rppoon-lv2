﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class FlexibleDiceRoller : IFlexibleDiceRoller, IClosedDiceRoller
    {
    
        private List<Die> dice;
        private List<int> resultForEachRoll;

        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public void RollAllDice() {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(Die.Roll());
            }
        }
        public void RemoveDiceWithXSides(int x) 
        {
            for (int i =0; i<dice.Count; i++)
            {
                resultForEachRoll.Clear();
                if (dice[i].NumberOfSides == x)
                {
                    dice.RemoveAt(i);
                }
            }
        }
    }
}
