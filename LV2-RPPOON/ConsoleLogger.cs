﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class ConsoleLogger : ILogger
    {
        public void Log(ILoggable data)
        {
            Console.WriteLine(data.GetStringRepresentation());
        }
    }
}
