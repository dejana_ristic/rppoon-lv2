﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    interface IFlexibleDiceRoller
    {
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
